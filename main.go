package main

import (
	"encoding/hex"
	"errors"
	"flag"
	"log"
)

type args struct {
	input   string
	output  string
	verbose bool
	quiet   bool
	irdFile string
	decKey  string
}

func process(a *args) error {
	if a.input == "" {
		return errors.New("missing input file")
	}

	isoData, err := newISO(a.input)
	if err != nil {
		return err
	}

	if err := isoData.selfCheck(); err != nil {
		return err
	}

	if a.verbose && !a.quiet {
		log.Printf("Info about ISO:\n%s", isoData)
	}

	var discKey []byte

	if a.decKey == "" {
		var irdData *ird

		if a.irdFile == "" {
			url, err := irdURLByGameID(isoData.gameID)
			if err != nil {
				return err
			}
			if !a.quiet {
				log.Printf("IRD URL: %s\n", url)
			}
			if irdData, err = irdFromURL(url); err != nil {
				log.Fatalf("error: %v\n", err)
			}
		} else if irdData, err = irdFromFile(a.irdFile); err != nil {
			return err
		}

		if err := isoData.irdCheck(irdData); err != nil {
			return err
		}

		if a.verbose && !a.quiet {
			log.Printf("Info from IRD:\n%s", irdData)
		}

		if discKey, err = makeDiscKey([]byte(irdData.data1)); err != nil {
			return err
		}

		if !a.quiet {
			log.Printf("Decrypting with disc key: %x\n", discKey)
		}
	} else { // a.decKey != ""
		src, err := hex.DecodeString(a.decKey)
		if err != nil {
			return err
		}
		if discKey, err = makeDiscKey(src); err != nil {
			return err
		}
	}

	var output string
	if a.output != "" {
		output = a.output
	} else {
		output = isoData.gameID + ".iso"
		if !a.quiet {
			log.Printf("Writing to file %s\n", output)
		}
	}

	if err := decrypt(isoData, discKey, a.input, output, a.quiet); err != nil {
		return err
	}

	log.Println("Decryption complete.")
	return nil
}

func main() {
	var a args

	flag.StringVar(&a.input, "iso", "", "Path to .iso file or stream")
	flag.StringVar(&a.input, "i", "", "Path to .iso file od stream (shorthand)")
	flag.StringVar(&a.output, "output", "", "output file name")
	flag.StringVar(&a.output, "o", "", "output file name (shorthand)")
	flag.StringVar(&a.irdFile, "ird", "", "Path to .ird file")
	flag.StringVar(&a.irdFile, "k", "", "Path to .ird file (shorthand)")
	flag.StringVar(&a.decKey, "decryption-key", "", "manually specify key")
	flag.StringVar(&a.decKey, "d", "", "manually specify key (shorthand)")
	flag.BoolVar(&a.verbose, "verbose", false, "verbose")
	flag.BoolVar(&a.verbose, "v", false, "verbose (shorthand)")
	flag.BoolVar(&a.quiet, "quiet", false, "quiet mode, only prints on error")
	flag.BoolVar(&a.quiet, "q", false, "quiet mode, only prints on error (shorthand)")

	flag.Parse()

	if err := process(&a); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
