package main

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

type region struct {
	start     int64
	end       int64
	encrypted bool
}

type iso struct {
	size    int64
	gameID  string
	regions []region
}

const (
	sectorSize   = 2048
	numInfoBytes = 4
)

func newISO(path string) (*iso, error) {

	s, err := size(path)
	if err != nil {
		return nil, err
	}

	if s == 0 {
		return nil, errors.New("looks like ISO/mount is empty")
	}

	input, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer input.Close()

	var numRegions int32
	if err := binary.Read(input, binary.BigEndian, &numRegions); err != nil {
		return nil, err
	}

	// Number of regions times two as the number represents
	// both encrypted and decrypted regions.
	numRegions *= 2

	// Skip unused bytes.
	if _, err := input.Seek(numInfoBytes, 1); err != nil {
		return nil, err
	}

	regions, err := readRegions(input, numRegions, s)
	if err != nil {
		return nil, err
	}

	// Seek to the start of sector 2. Offset + 16
	// skips a section containing some 'playstation'
	if _, err := input.Seek(sectorSize+16, 0); err != nil {
		return nil, err
	}

	data := make([]byte, 16)
	if _, err := io.ReadFull(input, data); err != nil {
		return nil, err
	}
	gameID := strings.TrimSpace(string(data))

	return &iso{
		size:    s,
		gameID:  gameID,
		regions: regions,
	}, nil
}

func (r *region) String() string {
	var encrypted string
	if r.encrypted {
		encrypted = "encrypted"
	} else {
		encrypted = "unecrypted"
	}
	return fmt.Sprintf(
		"[%d - %d] %s",
		r.start, r.end, encrypted)
}

func (i *iso) String() string {
	var sb strings.Builder
	fmt.Fprintf(&sb, "Game ID: %s\n", i.gameID)
	fmt.Fprintf(&sb, "ISO size: %d\n", i.size)
	fmt.Fprintf(&sb, "Number of regions: %d\n", len(i.regions))
	for j := range i.regions {
		fmt.Fprintf(&sb, "  %d: %s\n", j, &i.regions[j])
	}
	return sb.String()
}

func readRegions(
	input *os.File,
	numRegions int32,
	size int64,
) ([]region, error) {

	var encrypted bool

	regions := make([]region, 0, int(numRegions))

	for i := int32(0); i < numRegions; i++ {
		var start, end int32
		if err := binary.Read(input, binary.BigEndian, &start); err != nil {
			return nil, err
		}
		if err := binary.Read(input, binary.BigEndian, &end); err != nil {
			return nil, err
		}
		regions = append(regions, region{
			start:     int64(start) * sectorSize,
			end:       int64(end) * sectorSize,
			encrypted: encrypted,
		})

		// Every other (odd numbered) region is encrypted.
		encrypted = !encrypted

		if _, err := input.Seek(-numInfoBytes, 1); err != nil {
			return nil, err
		}
	}

	if len(regions) > 0 {
		// Last region might not actually be 2048 bytes,
		// so we'll just cheat.
		regions[len(regions)-1].end = size
	}

	return regions, nil
}

func size(path string) (int64, error) {
	fi, err := os.Stat(path)
	if err != nil {
		return 0, err
	}
	// Block device?
	if fi.Mode()&os.ModeDevice == os.ModeDevice &&
		fi.Mode()&os.ModeDevice != os.ModeCharDevice {
		f, err := os.Open(path)
		if err != nil {
			return 0, err
		}
		defer f.Close()
		pos, err := f.Seek(0, 2)
		if err != nil {
			return 0, err
		}
		return pos, nil
	}
	return fi.Size(), nil
}

func (isoData *iso) selfCheck() error {

	if len(isoData.regions) == 0 {
		return errors.New("ISO file has no regions")
	}

	if start := isoData.regions[len(isoData.regions)-1].start; start > isoData.size {
		const GiB = 1024 * 1024 * 1024
		return fmt.Errorf(
			"corrupt ISO: Expecting file size larger the %.2f GiB, "+
				"actual file size is %.2f GiB",
			float64(start)/GiB,
			float64(isoData.size)/GiB)
	}

	return nil
}

func (isoData *iso) irdCheck(irdData *ird) error {

	if len(irdData.regionHashes) != len(isoData.regions)-1 {
		return fmt.Errorf(
			"corrupt ISO or error in IRD. Regions %d (ISO) != %d (IRD)",
			len(isoData.regions)-1, len(irdData.regionHashes))
	}

	return nil
}
