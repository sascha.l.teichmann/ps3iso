package main

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"io"
	"os"

	"github.com/cheggaaa/pb/v3"
)

var (
	isoSecret = []byte{
		0x38, 0x0b, 0xcf, 0x0b,
		0x53, 0x45, 0x5b, 0x3c,
		0x78, 0x17, 0xab, 0x4f,
		0xa3, 0xba, 0x90, 0xed,
	}
	isoIV = []byte{
		0x69, 0x47, 0x47, 0x72,
		0xaf, 0x6f, 0xda, 0xb3,
		0x42, 0x74, 0x3a, 0xef,
		0xaa, 0x18, 0x62, 0x87,
	}
)

const (
	barTmpl = "{{ percent . }} {{ bar . }} {{ counters . }} [{{ rtime . }}<{{ etime . }}, {{ speed . }}]"
)

func makeDiscKey(src []byte) ([]byte, error) {
	block, err := aes.NewCipher(isoSecret)
	if err != nil {
		return nil, err
	}
	blockMode := cipher.NewCBCEncrypter(block, isoIV)
	dst := make([]byte, len(src))
	blockMode.CryptBlocks(dst, src)
	return dst, nil
}

func decrypt(
	iso *iso,
	discKey []byte,
	input, output string,
	quiet bool,
) error {

	key, err := aes.NewCipher(discKey)
	if err != nil {
		return err
	}

	inputIso, err := os.Open(input)
	if err != nil {
		return err
	}
	defer inputIso.Close()

	outputIso, err := os.Create(output)
	if err != nil {
		return err
	}
	// Do not defer closing outputIso

	// As block sizes on hdds/ssds tends to be at
	// least 4096 buffer a bit.
	out := bufio.NewWriterSize(outputIso, 10*sectorSize)

	inBuf := make([]byte, sectorSize)
	outBuf := make([]byte, sectorSize)

	iv := make([]byte, 16)

	var bar *pb.ProgressBar

	if !quiet {
		bar = pb.New(int(iso.size / sectorSize)).SetTemplateString(barTmpl).Start()
		defer bar.Finish()
	}

	for i := range iso.regions {
		region := &iso.regions[i]
		if _, err = inputIso.Seek(region.start, 0); err != nil {
			goto failed
		}

		// Unencrypted, just copy.
		if !region.encrypted {
			// To avoid copying buffers into buffers directly copy into output.
			if _, err = io.CopyN(outputIso, inputIso, region.end-region.start); err != nil {
				goto failed
			}
			if !quiet {
				bar.Add64((region.end - region.start) / sectorSize)
			}
		} else { // Encrypted
			for pos := region.start; pos < region.end; pos += sectorSize {
				var n int
				if pos+sectorSize <= region.end {
					n = sectorSize
				} else {
					n = int(region.end - pos)
				}
				if _, err = io.ReadFull(inputIso, inBuf[:n]); err != nil {
					goto failed
				}
				for j := n; j < sectorSize; j++ {
					inBuf[j] = 0
				}
				num := pos / sectorSize
				for j := range iv {
					iv[15-j] = byte(num)
					num >>= 8
				}
				dec := cipher.NewCBCDecrypter(key, iv)
				dec.CryptBlocks(outBuf, inBuf)
				if _, err = out.Write(outBuf[:n]); err != nil {
					goto failed
				}
				if !quiet {
					bar.Increment()
				}
			}
		}
		// Flush here to re-enable direct copying again.
		if err = out.Flush(); err != nil {
			goto failed
		}
	}

failed:
	if err2 := outputIso.Close(); err2 != nil && err != nil {
		err = err2
	}

	return err
}
