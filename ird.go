package main

import (
	"bytes"
	"compress/gzip"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type fileHash struct {
	key   int64
	value string
}

type ird struct {
	version       byte
	gameID        string
	gameName      string
	updateVersion string
	gameVersion   string
	appVersion    string
	identifier    string
	header        string
	footer        string
	regionHashes  []string
	fileHashes    []fileHash
	pic           string
	data1         string
	data2         string
	uid           int32
}

const irdMagic = "3IRD"

const (
	allIRDLocation = "http://jonnysp.bplaced.net/data.php"
	getIRDLocation = "http://jonnysp.bplaced.net/ird/"
)

func read(r io.Reader, n int) (string, error) {
	data := make([]byte, n)
	if _, err := io.ReadFull(r, data); err != nil {
		return "", err
	}
	return string(data), nil
}

func newIRD(r io.Reader) (*ird, error) {

	magic, err := read(r, 4)
	if err != nil {
		return nil, err
	}
	if magic != irdMagic {
		return nil, errors.New("not an IRD file, corruped IRD file, or unknown IRD format")
	}
	var version byte
	if err := binary.Read(r, binary.LittleEndian, &version); err != nil {
		return nil, err
	}

	gameID, err := read(r, 9)
	if err != nil {
		return nil, err
	}

	nameLength, err := sevenBitEncodedInt32(r)
	if err != nil {
		return nil, err
	}
	gameName, err := read(r, int(nameLength))
	if err != nil {
		return nil, err
	}

	updateVersion, err := read(r, 4)
	if err != nil {
		return nil, err
	}

	gameVersion, err := read(r, 5)
	if err != nil {
		return nil, err
	}

	appVersion, err := read(r, 5)
	if err != nil {
		return nil, err
	}

	var identifier string
	if version == 7 {
		if identifier, err = read(r, 4); err != nil {
			return nil, err
		}
	}

	var headerLength int32
	if err := binary.Read(r, binary.LittleEndian, &headerLength); err != nil {
		return nil, err
	}
	header, err := read(r, int(headerLength))
	if err != nil {
		return nil, err
	}

	var footerLength int32
	if err := binary.Read(r, binary.LittleEndian, &footerLength); err != nil {
		return nil, err
	}
	footer, err := read(r, int(footerLength))
	if err != nil {
		return nil, err
	}

	var regionCount byte
	if err := binary.Read(r, binary.LittleEndian, &regionCount); err != nil {
		return nil, err
	}

	regionHashes := make([]string, int(regionCount))
	for i := range regionHashes {
		if regionHashes[i], err = read(r, 16); err != nil {
			return nil, err
		}
	}

	var fileCount int32
	if err := binary.Read(r, binary.LittleEndian, &fileCount); err != nil {
		return nil, err
	}

	fileHashes := make([]fileHash, int(fileCount))
	for i := range fileHashes {
		var key int64
		if err := binary.Read(r, binary.LittleEndian, &key); err != nil {
			return nil, err
		}
		value, err := read(r, 16)
		if err != nil {
			return nil, err
		}
		fileHashes[i] = fileHash{key, value}
	}

	var pic string
	if version >= 9 {
		if pic, err = read(r, 115); err != nil {
			return nil, err
		}
	}

	var dummy int32
	if err := binary.Read(r, binary.LittleEndian, &dummy); err != nil {
		return nil, err
	}

	data1, err := read(r, 16)
	if err != nil {
		return nil, err
	}
	data2, err := read(r, 16)
	if err != nil {
		return nil, err
	}

	if version < 9 {
		if pic, err = read(r, 115); err != nil {
			return nil, err
		}
	}

	var uid int32
	if err := binary.Read(r, binary.LittleEndian, &uid); err != nil {
		return nil, err
	}

	return &ird{
		version:       version,
		gameID:        gameID,
		gameName:      gameName,
		updateVersion: updateVersion,
		gameVersion:   gameVersion,
		appVersion:    appVersion,
		identifier:    identifier,
		header:        header,
		footer:        footer,
		regionHashes:  regionHashes,
		fileHashes:    fileHashes,
		pic:           pic,
		data1:         data1,
		data2:         data2,
		uid:           uid,
	}, nil
}

func (i *ird) String() string {
	var sb strings.Builder
	fmt.Fprintf(&sb, "Version: %d\n", i.version)
	fmt.Fprintf(&sb, "Game ID: %s\n", i.gameID)
	fmt.Fprintf(&sb, "Game Name: %s\n", i.gameName)
	fmt.Fprintf(&sb, "Update Version: %s\n", i.updateVersion)
	fmt.Fprintf(&sb, "Game Version: %s\n", i.gameVersion)
	fmt.Fprintf(&sb, "App Version: %s\n", i.appVersion)
	fmt.Fprintf(&sb, "Region Count: %d\n", len(i.regionHashes))
	fmt.Fprintf(&sb, "File Count: %d\n", len(i.fileHashes))
	fmt.Fprintf(&sb, "Data1: %x\n", i.data1)
	fmt.Fprintf(&sb, "Data2: %x\n", i.data2)
	return sb.String()
}

func irdURLByGameID(gameID string) (string, error) {

	client := http.Client{
		Timeout: time.Second * 5,
	}

	res, err := client.Get(allIRDLocation)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return "", fmt.Errorf("status code error: %d %s",
			res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return "", err
	}

	gameID = strings.ReplaceAll(gameID, "-", "")

	var url string

	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		if v, ok := s.Attr("href"); ok && v != "" {
			parts := strings.Split(v, "/")
			v = parts[len(parts)-1]
			v = strings.ReplaceAll(v, `\"`, ``)
			if strings.Contains(v, gameID) {
				url = v
			}
		}
	})

	if url == "" {
		return "", fmt.Errorf("cannot find URL for game id %s", gameID)
	}

	return getIRDLocation + url, nil
}

func downloadURL(url string) ([]byte, error) {

	client := http.Client{
		Timeout: time.Second * 5,
	}

	res, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s",
			res.StatusCode, res.Status)
	}

	var buf bytes.Buffer
	if _, err := io.Copy(&buf, res.Body); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func irdFromURL(url string) (*ird, error) {
	data, err := downloadURL(url)
	if err != nil {
		return nil, err
	}
	return irdFromBytes(data)
}

func irdFromFile(fname string) (*ird, error) {
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}
	return irdFromBytes(data)
}

func irdFromBytes(b []byte) (*ird, error) {

	// Assume it is uncompressed if we can see the magic.
	if bytes.HasPrefix(b, []byte(irdMagic)) {
		return newIRD(bytes.NewReader(b))
	}

	r, err := gzip.NewReader(bytes.NewReader(b))
	if err != nil {
		return nil, err
	}
	return newIRD(r)
}

func sevenBitEncodedInt32(r io.Reader) (int32, error) {

	var value, shift int32
	b := byte(0xff)

	for b&0x80 != 0 {
		if shift == 5*7 {
			return 0, errors.New("corrupt stream")
		}
		if err := binary.Read(r, binary.LittleEndian, &b); err != nil {
			return 0, err
		}
		value |= int32(b&0x7f) << shift
		shift += 7
	}

	return value, nil
}
