module gitlab.com/sascha.l.teichmann/ps3iso

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/cheggaaa/pb/v3 v3.0.5
)
